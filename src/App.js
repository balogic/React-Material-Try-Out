import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import RaisedButton from 'material-ui/RaisedButton';
import AutoComplete from 'material-ui/AutoComplete';
import AppBar from 'material-ui/AppBar';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      dataSource: []
    }
  }
  update(value) {
    this.setState({
      dataSource: [
        value, value + value,
        value + value + value
      ]
    })
  }
  render() {
    return (
      <div>
        <MuiThemeProvider>
          <AppBar title="Material UI Sample"/>
          <br></br>
          <h2>Search Something</h2>
          <br></br>
          <AutoComplete hintText="Type Something" dataSource={this.state.dataSource} onUpdateInput={this.update.bind(this)}/>
          <br></br>
          <RaisedButton label="Search" primary={true}/>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default App;
